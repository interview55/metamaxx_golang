package main

import "fmt"

func MergeSlices(c1 []string, c2 []string) (c3 []string) {

	fSlice := append(c1, c2...)
	uSlice := make(map[string]bool)

	for _, val := range fSlice {
		if _, ok := uSlice[val]; !ok {
			uSlice[val] = true
			c3 = append(c3, val)
		}
	}
	return
}

func main() {
	c1 := []string{"Red", "Black", "White"}
	c2 := []string{"Black", "Yellow", "Orange"}

	fmt.Println(MergeSlices(c1, c2))
}
