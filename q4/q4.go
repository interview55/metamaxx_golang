package main

import (
	"fmt"
	"sync"
)

var x = 0

func increment(dataChan chan int, wg *sync.WaitGroup) {
	dataChan <- 1
	x = x + 1
	<-dataChan
	wg.Done()
}
func main() {
	var w sync.WaitGroup
	dataChan := make(chan int, 1000)

	for i := 0; i < 1000; i++ {
		w.Add(1)
		go increment(dataChan, &w)
	}

	w.Wait()
	fmt.Println("final value of x", x)
}
