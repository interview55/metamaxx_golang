package main

import (
	"fmt"
	"sync"
)

func main() {
	dataBits := []int{10, 20, 35, 100, 200, 502}

	LoopOverChannel(dataBits)

}

func LoopOverChannel(dataBits []int) {
	dataChan := make(chan int)
	var wg sync.WaitGroup
	go func() {
		for _, v := range dataBits {
			wg.Add(1)
			dataChan <- v
			wg.Done()
		}
		wg.Wait()
		close(dataChan)
	}()

	for bit := range dataChan {
		fmt.Println(bit)
	}

}
