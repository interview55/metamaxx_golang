package httperror

import (
	"net/http"
)

const ErrCodeServiceUnavailable = "STATUS_503"

var ErrServiceUnavailableMsg = "service_unavailable"

type AppError struct {
	errCode   string
	errString string
}

func (e AppError) Error() string {
	return e.errCode + ": " + e.errString
}

func (e AppError) Code() string {
	return e.errCode
}

func (e AppError) Message() string {
	return e.errString
}

func NewAppError(code, msg string) AppError {
	return AppError{
		errCode:   code,
		errString: msg,
	}
}

type HttpError struct {
	cause      error // actual stack of error
	httpCode   int
	errCode    string //  app standard error code
	message    string
	errContext interface{} // optional errContext
}

// WrapHttpError adds a HTTP code to an existing error.
func WrapHttpError(err error, code int, errCode string, msg string, errCtx interface{}) error {
	if err == nil {
		return nil
	}
	return &HttpError{cause: err, httpCode: code, errCode: errCode, message: msg, errContext: errCtx}
}

func (w *HttpError) Error() string { return w.cause.Error() }

func ServiceUnavailableError(cause error, errMsg string) error {
	if cause == nil {
		cause = NewAppError(ErrCodeServiceUnavailable, ErrServiceUnavailableMsg)
	}
	return WrapHttpError(cause, http.StatusServiceUnavailable, ErrCodeServiceUnavailable, errMsg, nil)
}
