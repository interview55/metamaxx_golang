package main

import (
	"fmt"
	"metamaxxInterview/q6/httperror"
	"net/http"
)

func main() {
	response, err := http.Get("https://httpstat.us/503")
	if err != nil {
		return
	}
	defer response.Body.Close()
	if response.StatusCode == http.StatusServiceUnavailable {
		herr := httperror.ServiceUnavailableError(err, "")
		if herr != nil {
			fmt.Println(herr.Error())
			return
		}
		return
	}
}
