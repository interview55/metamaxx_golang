package q5_test

import (
	q5 "metamaxxInterview/q5"
	"testing"
)

type MultiTest struct {
	num1, num2, expect int
}

var addMultiTests = []MultiTest{
	MultiTest{1, 2, 3},
	MultiTest{3, 4, 7},
	MultiTest{5, 6, 11},
}

var subtractMultiTests = []MultiTest{
	MultiTest{2, 1, 1},
	MultiTest{4, 3, 1},
	MultiTest{6, 5, 0},
}

func TestAdd(t *testing.T) {

	for _, MultiTest := range addMultiTests {
		if output := q5.Add(MultiTest.num1, MultiTest.num2); output != MultiTest.expect {
			t.Errorf("Result value %v != expected value %v", output, MultiTest.expect)
		}
	}
}

func TestSubtract(t *testing.T) {
	for _, MultiTest := range subtractMultiTests {
		if output := q5.Subtract(MultiTest.num1, MultiTest.num2); output != MultiTest.expect {
			t.Errorf("Result value %v != expected value %v", output, MultiTest.expect)
		}
	}
}
